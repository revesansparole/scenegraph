"""
Plot evolution of cube along trajectory
"""
from math import radians

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Polygon

from scenegraph import Rotation, ScNode, Scene, Translation
from scenegraph.triangle_set import cube

# create scene
sc = Scene()
node = ScNode(shape=cube())
node.add_transfo(Rotation(alpha=0., axis='Oz', name="rot"))
node.add_transfo(Translation(vec=np.array([0., 0., 0.]), name="trans"))
nid = sc.add(node)

# compute evolution
evol = []

for t in range(5):
    vec = np.array([t * 2, t, 0.])
    # modify transformation
    trans = node.get_transfo('trans')
    trans.vec = vec

    rot = node.get_transfo('rot')
    rot.alpha = radians(t * 15)

    # apply
    mesh = sc.shape(nid).copy()
    mesh.apply(sc.transfo(nid, absolute=True))

    evol.append((t, vec, mesh))

# plot
fig, axes = plt.subplots(1, 1, figsize=(8, 6), squeeze=False)
ax = axes[0, 0]
for i, (t, vec, mesh) in enumerate(evol):
    crv, = ax.plot([vec[0]], [vec[1]], 'o', label=f"{t:d}")

    for face in mesh.faces():
        pts = np.array([mesh[ind][:2] for ind in face])
        axes[0, 0].add_patch(Polygon(pts, closed=False, facecolor=crv.get_color()))

ax.plot([vec[0] for _, vec, _ in evol], [vec[1] for _, vec, _ in evol], "o-", color="#000000")

ax.legend(loc='upper left')
ax.set_xlim(-0.5, 10)
ax.set_ylim(-0.5, 10)
ax.set_aspect(1)
ax.set_xlabel("x")
ax.set_ylabel("y")

fig.tight_layout()
plt.show()
