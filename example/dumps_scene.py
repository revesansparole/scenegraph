"""
Simple scene
"""
import json
from math import radians

import numpy as np

from scenegraph import Rotation, RotationEuler, ScNode, Scaling, Scene, Translation
from scenegraph.io import dumps
from scenegraph.triangle_set import cube

# create scene
sc = Scene()
shp = cube()
shp.meta['color'] = '#0000aa'
sc.add_def(shp, 'cube')

gr = ScNode()
gr.add_transfo((Translation(vec=[1, 2, 3])))
gid = sc.add(gr)

n0 = ScNode(shape='cube')
n0.add_transfo(Translation(vec=[1, 2, 3], name='trans'))
n0.meta['color'] = '#000000'
sc.add(n0, pid=gid)

n1 = ScNode(shape='cube')
n1.add_transfo(Scaling(vec=[1, 2, 3]))
n1.add_transfo(Translation(vec=[1, -2, 3]))
sc.add(n1, pid=gid)

n2 = ScNode(shape='cube')
n2.add_transfo(Rotation(alpha=radians(30), axis='Ox'))
n2.add_transfo(Translation(vec=[-1, 2, 3]))
sc.add(n2, pid=gid)

n3 = ScNode(shape='cube')
n3.add_transfo(Rotation(alpha=radians(30), axis='Oy'))
n3.add_transfo(Translation(vec=[-3, 2, 3]))
sc.add(n3, pid=gid)

n4 = ScNode(shape='cube')
n4.add_transfo(Rotation(alpha=radians(30), axis='Oz'))
n4.add_transfo(Translation(vec=[-5, 2, 3]))
sc.add(n4, pid=gid)

n5 = ScNode(shape='cube')
axis = np.array([1., 1., 1.])
axis /= np.dot(axis, axis)
n5.add_transfo(Rotation(alpha=radians(30), axis=axis))
n5.add_transfo(Translation(vec=[5, 5, 5]))
nid5 = sc.add(n5)

nn0 = ScNode(shape=cube())
nn0.add_transfo(Rotation(alpha=radians(30), axis='Oz'))
nn0.add_transfo(Translation(vec=[1, 0, 0]))
nn0.meta['color'] = '#00aa00'
sc.add(nn0,pid=nid5)

n6 = ScNode(shape=cube())
n6.add_transfo(RotationEuler(pitch=radians(0), roll=radians(0), yaw=radians(10)))
n6.add_transfo(Translation(vec=[-1, -2, 3]))
sc.add(n6)

# export
scs = dumps(sc)

json.dump(scs, open("sc.json", 'w'), indent=2)
