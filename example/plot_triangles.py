"""
Triangles in space
==================

Example of triangles in space
"""
from math import radians

import matplotlib.pyplot as plt
import numpy as np
from scenegraph.transfo import Rotation
from scenegraph.triangle import Triangle

# create scene
scn = {}
scn['t0'] = Triangle(np.array([1, 1, 1]), np.array([2, 1, 1]), np.array([1, 2, 1]))
scn['t1'] = Triangle(np.array([0, 1, 0]), np.array([0, 2, 0]), np.array([0, 1, 1]))

scn['t0_rot_oz'] = Triangle(*scn['t0'].points())
scn['t0_rot_oz'].apply(Rotation(alpha=radians(30), axis='Oz').matrix())
scn['t1_rot_oz'] = Triangle(*scn['t1'].points())
scn['t1_rot_oz'].apply(Rotation(alpha=radians(20), axis='Oz').matrix())

scn['t0_rot_oy'] = Triangle(*scn['t0'].points())
scn['t0_rot_oy'].apply(Rotation(alpha=radians(10), axis='Oy').matrix())
scn['t1_rot_ox'] = Triangle(*scn['t1'].points())
scn['t1_rot_ox'].apply(Rotation(alpha=radians(20), axis='Ox').matrix())

# plot
fig, axes = plt.subplots(2, 2, figsize=(10, 8), squeeze=False)
ax = axes[1, 0]
ax.set_title("Oxy")
for _, tr in scn.items():
    pts = list(tr.points())
    pts += [pts[0]]  # close polygon
    ax.plot([x for x, _, _ in pts], [y for _, y, _ in pts])

ax.set_aspect(1.)

ax = axes[1, 1]
ax.set_title("Oyz")
for _, tr in scn.items():
    pts = list(tr.points())
    pts += [pts[0]]  # close polygon
    ax.plot([y for _, y, _ in pts], [z for _, _, z in pts])

ax.set_aspect(1.)

ax = axes[0, 0]
ax.set_title("Oxz")
for name, tr in scn.items():
    pts = list(tr.points())
    pts += [pts[0]]  # close polygon
    ax.plot([x for x, _, _ in pts], [z for _, _, z in pts], label=name)

ax.legend(loc='upper right')
ax.set_aspect(1.)

axes[0, 1].set_visible(False)

fig.tight_layout()
plt.show()
