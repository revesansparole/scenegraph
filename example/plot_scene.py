"""
Simple scene
"""
import json
from pathlib import Path
from subprocess import run

import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colormaps as cmap
from matplotlib.patches import Polygon
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from scenegraph.algo import bounding_box
from scenegraph.io import loads

# load scene
pth = Path("sc.json")
if not pth.exists():
    run(["python", "dumps_scene.py"])

sc = loads(json.load(pth.open()))

# project all scene into single mesh for display purpose
meshes = []
for nid, node in sc.nodes():
    if node.shape is not None:
        mesh = sc.shape(nid)
        try:
            color = mesh.meta['color']
        except KeyError:
            color = sc.meta(nid, 'color', '#aaaaaa')
        mesh = mesh.copy()
        mesh.apply(sc.transfo(nid, absolute=True))
        meshes.append([mesh, color])

# compute bounding box
bb_min, bb_max = bounding_box(sc)

iso_min = min(bb_min)
iso_max = max(bb_max)

# plot
fig = plt.figure(figsize=(9, 9))
gs = gridspec.GridSpec(nrows=2, ncols=2)

axes = [[fig.add_subplot(gs[0, 0]), fig.add_subplot(gs[0, 1])],
        [fig.add_subplot(gs[1, 0]), fig.add_subplot(gs[1, 1], projection='3d')]]
axes = np.array(axes)

for i, (mesh, col) in enumerate(meshes):
    for face in mesh.faces():
        pts = np.array([mesh[ind][:2] for ind in face])
        axes[0, 0].add_patch(Polygon(pts, closed=False, facecolor=cmap['tab10'](i % 10), edgecolor=col))
        pts = np.array([mesh[ind][1:] for ind in face])
        axes[0, 1].add_patch(Polygon(pts, closed=False, facecolor=cmap['tab10'](i % 10), edgecolor=col))
        pts = np.array([mesh[ind][[0, 2]] for ind in face])
        axes[1, 0].add_patch(Polygon(pts, closed=False, facecolor=cmap['tab10'](i % 10), edgecolor=col))

for shp, col in meshes:
    pts = shp.points()
    axes[1, 1].add_collection3d(Poly3DCollection([tuple(pts[i] for i in tr) for tr in shp.faces()],
                                                 facecolors=col, linewidths=1))

ax = axes[0, 0]
ax.set_xlim(iso_min, iso_max)
ax.set_ylim(iso_min, iso_max)
ax.set_xlabel("x")
ax.set_ylabel("y")

ax = axes[0, 1]
ax.set_xlim(iso_min, iso_max)
ax.set_ylim(iso_min, iso_max)
ax.set_xlabel("y")
ax.set_ylabel("z")

ax = axes[1, 0]
ax.set_xlim(iso_min, iso_max)
ax.set_ylim(iso_min, iso_max)
ax.set_xlabel("x")
ax.set_ylabel("z")

ax = axes[1, 1]
ax.set_xlim(iso_min, iso_max)
ax.set_ylim(iso_min, iso_max)
ax.set_zlim(iso_min, iso_max)

fig.tight_layout()
plt.show()
