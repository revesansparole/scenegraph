from pytest import fixture
from scenegraph.topo.graph import Graph


@fixture()
def g():
    g = Graph()
    for i in range(10):
        g.add_vertex(i)
    for i in range(9):
        g.add_edge(i, i + 1, i)

    yield g

    g.clear()


# ##########################################################
#
# Graph concept
#
# ##########################################################
def test_source(g):
    for i in range(9):
        assert g.source(i) == i


def test_target(g):
    for i in range(9):
        assert g.target(i) == (i + 1)


def test_has_vertex(g):
    for i in range(10):
        assert g.has_vertex(i)


def test_has_edge(g):
    for i in range(9):
        assert g.has_edge(i)


# ##########################################################
#
# Vertex List Graph Concept
#
# ##########################################################
def test_vertices(g):
    assert list(g.vertices()) == list(range(10))


def test_nb_vertices(g):
    assert g.nb_vertices() == 10


def test_in_neighbors(g):
    for i in range(9):
        assert list(g.in_neighbors(i + 1)) == [i]


def test_out_neighbors(g):
    for i in range(9):
        assert list(g.out_neighbors(i)) == [i + 1]


def test_neighbors(g):
    for i in range(8):
        neis = list(g.neighbors(i + 1))
        assert i in neis
        assert i + 2 in neis


def test_nb_in_neighbors(g):
    for i in range(9):
        assert g.nb_in_neighbors(i + 1) == 1


def test_nb_out_neighbors(g):
    for i in range(9):
        assert g.nb_out_neighbors(i) == 1


def test_nb_neighbors(g):
    for i in range(8):
        assert g.nb_neighbors(i + 1) == 2


def test_edge(g):
    assert g.edge(0, 1) == 0
    assert g.edge(0, 2) is None


# ##########################################################
#
# Edge List Graph Concept
#
# ##########################################################
def test_edges(g):
    assert list(g.edges()) == list(range(9))


def test_in_edges(g):
    for i in range(9):
        assert list(g.in_edges(i + 1)) == [i]


def test_out_edges(g):
    for i in range(9):
        assert list(g.out_edges(i)) == [i]


def test_vertex_edges(g):
    for i in range(8):
        neis = list(g.edges(i + 1))
        assert i in neis
        assert i + 1 in neis


def test_nb_in_edges(g):
    for i in range(9):
        assert g.nb_in_edges(i + 1) == 1


def test_nb_out_edges(g):
    for i in range(9):
        assert g.nb_out_edges(i) == 1


def test_nb_edges(g):
    assert g.nb_edges() == 9
    for i in range(8):
        assert g.nb_edges(i + 1) == 2


# ##########################################################
#
# Mutable Vertex Graph concept
#
# ##########################################################
def test_add_vertex(g):
    assert g.add_vertex(100) == 100
    vid = g.add_vertex()
    assert g.has_vertex(vid)


def test_remove_vertex(g):
    g.remove_vertex(5)
    assert not g.has_vertex(5)
    assert not g.has_edge(4)
    assert not g.has_edge(5)
    assert 5 not in list(g.neighbors(6))
    assert 5 not in list(g.neighbors(4))


def test_clear(g):
    g.clear()
    assert g.nb_vertices() == 0
    assert g.nb_edges() == 0


# ##########################################################
#
# Mutable Edge Graph concept
#
# ##########################################################
def test_add_edge(g):
    assert g.add_edge(0, 9, 100) == 100
    eid = g.add_edge(2, 1)
    assert eid in list(g.in_edges(1))
    assert eid in list(g.out_edges(2))


def test_remove_edge(g):
    g.remove_edge(4)
    assert not g.has_edge(4)
    assert 4 not in list(g.neighbors(5))
    assert 5 not in list(g.neighbors(4))


def test_clear_edges(g):
    g.clear_edges()
    assert g.nb_vertices() == 10
    assert g.nb_edges() == 0


# ##########################################################
#
# Extend Graph concept
#
# ##########################################################
def test_extend(g):
    trans_vid, trans_eid = g.extend(g)
    assert len(trans_vid) == 10
    assert len(trans_eid) == 9
