========================
scenegraph
========================

.. {# pkglts, doc

.. image:: https://revesansparole.gitlab.io/scenegraph/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/scenegraph/1.0.0/

.. image:: https://revesansparole.gitlab.io/scenegraph/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/scenegraph

.. image:: https://revesansparole.gitlab.io/scenegraph/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://revesansparole.gitlab.io/scenegraph/

.. image:: https://badge.fury.io/py/scenegraph.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/scenegraph

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/revesansparole/scenegraph/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/revesansparole/scenegraph/commits/main

.. |main_coverage| image:: https://gitlab.com/revesansparole/scenegraph/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/revesansparole/scenegraph/commits/main
.. #}

Simple organisation of shapes in space

